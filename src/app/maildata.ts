export interface MailData {
  id: number;
  name: string;
  topic: string;
  date: string;
  detail: string;
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubheadermailComponent } from './subheadermail.component';

describe('SubheadermailComponent', () => {
  let component: SubheadermailComponent;
  let fixture: ComponentFixture<SubheadermailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubheadermailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubheadermailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { HeaderMail } from './headermail';

@Component({
  selector: 'app-subheadermail',
  templateUrl: './subheadermail.component.html',
  styleUrls: ['./subheadermail.component.css'],
})
export class SubheadermailComponent implements OnInit {
  headermail: HeaderMail[];

  constructor() {}

  ngOnInit(): void {
    this.headermail = [
      {
        name: 'Primary',
        icon:
          'https://www.gstatic.com/images/icons/material/system/2x/inbox_gm_googlered600_20dp.png',
      },
      {
        name: 'Social',
        icon:
          'https://www.gstatic.com/images/icons/material/system/2x/people_black_20dp.png',
      },
      {
        name: 'Promotions',
        icon:
          'https://www.gstatic.com/images/icons/material/system/2x/local_offer_black_20dp.png',
      },
      {
        name: '',
        icon: '',
      },
    ];
  }
}

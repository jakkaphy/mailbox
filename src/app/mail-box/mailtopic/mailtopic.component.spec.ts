import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailtopicComponent } from './mailtopic.component';

describe('MailtopicComponent', () => {
  let component: MailtopicComponent;
  let fixture: ComponentFixture<MailtopicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailtopicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailtopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { MailData } from '../../maildata';

@Component({
  selector: 'app-mailtopic',
  templateUrl: './mailtopic.component.html',
  styleUrls: ['./mailtopic.component.css'],
})
export class MailtopicComponent implements OnInit {
  @Input() datamail: MailData;

  constructor() {}

  ngOnInit(): void {}
}

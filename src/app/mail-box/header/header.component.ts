import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @Output() onSearch = new EventEmitter<string>();
  // searchInput = '';
  constructor(private router: Router, private location: Location) {}
  setVisible = false;

  ngOnInit(): void {}

  addInput(value) {
    // this.searchInput = value;
    this.onSearch.emit(value);
    if (value !== '') {
      this.setVisible = true;
      this.router.navigate(['/mailbox'], {
        queryParams: { search: `${value}` },
      });
    } else {
      this.setVisible = false;
      this.location.replaceState('/mailbox');
    }
  }
  refresh(): void {
    this.setVisible = false;
    this.location.replaceState('/mailbox');
    window.location.reload();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SubheaderComponent } from './subheader/subheader.component';
import { ContentComponent } from './content/content.component';
import { RightsideComponent } from './rightside/rightside.component';
import { LeftsideComponent } from './leftside/leftside.component';
import { SubheadermailComponent } from './subheadermail/subheadermail.component';
import { MailtopicComponent } from './mailtopic/mailtopic.component';
import { MaildetailComponent } from './maildetail/maildetail.component';

import { ScrollPanelModule } from 'primeng/scrollpanel';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';

import { MailBoxRoutingModule } from './mail-box-routing.module';
import { SearchFilterPipe } from './search-filter.pipe';

@NgModule({
  declarations: [
    HeaderComponent,
    SubheaderComponent,
    ContentComponent,
    RightsideComponent,
    LeftsideComponent,
    SubheadermailComponent,
    MailtopicComponent,
    MaildetailComponent,
    SearchFilterPipe
  ],
  imports: [
    CommonModule,
    ScrollPanelModule,
    ButtonModule,
    FormsModule,
    DropdownModule,
    MailBoxRoutingModule
  ],
  exports: [
    HeaderComponent,
    SubheaderComponent,
    ContentComponent,
    RightsideComponent,
    LeftsideComponent,
    SubheadermailComponent,
    MailtopicComponent,
    MaildetailComponent,
  ]
})
export class MailBoxModule { }

import { Component, OnInit, Input } from '@angular/core';
import { MailData } from '../../maildata';
import { Location } from '@angular/common';

@Component({
  selector: 'app-maildetail',
  templateUrl: './maildetail.component.html',
  styleUrls: ['./maildetail.component.css'],
})
export class MaildetailComponent implements OnInit {
  @Input() datadetail: MailData;
  constructor(private location: Location) {}

  ngOnInit(): void {}
  onBack() {
    this.location.back();
  }
}

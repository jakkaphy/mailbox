import { Component, OnInit } from '@angular/core';
import { MailData } from '../../maildata';
import { MailService } from '../../services/mailservice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css'],
})
export class ContentComponent implements OnInit {
  list: string[];
  routerurl: string;
  id: number;
  searchHeader: string;
  caseA: boolean;
  caseB: boolean;
  constructor(
    private mailservice: MailService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
  ) {
  }

  maildata: MailData[];
  maildetail: MailData;
  ngOnInit(): void {
    this.list = [
      'all',
      'Not choosing at all',
      'Read',
      'Unread',
      'Star',
      'Not starred',
    ];
    this.maildata = this.mailservice.getMailData();
    this.routerurl = this.router.url;

    // this.caseA = this.router.url === '/mailbox' || this.router.url === `/mailbox?search=${this.searchHeader}`;
    // if (this.routerurl !== '/mailbox') {
      // this.caseA = true;

    if (this.route.snapshot.paramMap.get('id')) {
      this.id = +this.route.snapshot.paramMap.get('id');
      this.mailservice
        .getMailId(this.id)
        .subscribe((element) => (this.maildetail = element));
      this.caseA = false;

    } else if (this.routerurl === '/mailbox'){
      this.caseA = true;
    } else {
      this.location.replaceState('/mailbox');
      this.caseA = true;
    }

    // if (this.router.url === `/mailbox?search=${this.searchHeader}`){
    //   this.caseA = true;
    //   console.log(true);
    // }
    // console.log(this.caseA);

  }
  onSelect(data) {
    this.maildetail = data;
    this.router.navigate(['/mailbox/detail', data.id]);
  }
  searchIn(value) {
    this.searchHeader = value;

  }

  // caseMailBox = this.caseA || this.caseB;
}

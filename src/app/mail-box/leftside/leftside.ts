export interface LeftSideMenu {
  name: string;
  qty?: number;
  icon: string;
}

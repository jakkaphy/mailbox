import { Component, OnInit } from '@angular/core';
import { LeftSideMenu } from './leftside';

@Component({
  selector: 'app-leftside',
  templateUrl: './leftside.component.html',
  styleUrls: ['./leftside.component.css'],
})
export class LeftsideComponent implements OnInit {
  constructor() {}
  leftSideMenu: LeftSideMenu[];
  subCateMenu: string[];

  ngOnInit(): void {
    this.leftSideMenu = [
      {
        name: 'Inbox',
        qty: 873,
        icon: 'https://www.gstatic.com/images/icons/material/system/2x/inbox_gm_googlered600_20dp.png'
      },
      {
        name: 'Starred',
        icon: 'https://www.gstatic.com/images/icons/material/system/2x/grade_black_20dp.png'
      },
      {
        name: 'Snoozed',
        icon: 'https://www.gstatic.com/images/icons/material/system/2x/watch_later_black_20dp.png'
      },
      {
        name: 'Important',
        icon: 'https://www.gstatic.com/images/icons/material/system/2x/label_important_black_20dp.png'
      },
      {
        name: 'Send',
        icon: 'https://www.gstatic.com/images/icons/material/system/2x/send_black_20dp.png'
      },
      {
        name: 'Drafts',
        qty: 8,
        icon: 'https://www.gstatic.com/images/icons/material/system/2x/insert_drive_file_black_20dp.png'
      },
      {
        name: 'Categories',
        icon: 'https://www.gstatic.com/images/icons/material/system/2x/label_important_black_20dp.png'
      },
    ];
    this.subCateMenu = ['Social', 'Updates', 'Forums', 'Promotions'];
  }
}

// tslint:disable-next-line: class-name

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subheader',
  templateUrl: './subheader.component.html',
  styleUrls: ['./subheader.component.css'],
})
export class SubheaderComponent implements OnInit {
  list: string[];
  constructor() {}

  ngOnInit(): void {
    this.list = [
      'all',
      'Not choosing at all',
      'Read',
      'Unread',
      'Star',
      'Not starred',
    ];
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import { MailData } from '../maildata';

@Pipe({
  name: 'searchFilter',
})
export class SearchFilterPipe implements PipeTransform {
  transform(maildata: MailData[], searchHeader: string): MailData[] {
    if (!maildata || !searchHeader) {
      return maildata;
    }
    // tslint:disable-next-line: max-line-length
    return maildata.filter(maildata => maildata.name.toLocaleLowerCase().indexOf(searchHeader.toLocaleLowerCase()) !== -1 || maildata.topic.toLocaleLowerCase().indexOf(searchHeader.toLocaleLowerCase()) !== -1);

  }
}

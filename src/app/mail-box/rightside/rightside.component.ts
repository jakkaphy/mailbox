import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rightside',
  templateUrl: './rightside.component.html',
  styleUrls: ['./rightside.component.css'],
})
export class RightsideComponent implements OnInit {
  constructor() {}
  rightSideMenu: string[];
  ngOnInit(): void {
    this.rightSideMenu = [
      'https://www.gstatic.com/companion/icon_assets/calendar_2x.png',
      'https://www.gstatic.com/companion/icon_assets/keep_2x.png',
      'https://www.gstatic.com/companion/icon_assets/tasks2_2x.png',
      'https://www.gstatic.com/images/icons/material/system/1x/add_grey600_24dp.png'
    ];
  }
}

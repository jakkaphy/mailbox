import { Injectable } from '@angular/core';
import { MailData } from '../maildata';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MailService {
  constructor() {}
  getMailData(): MailData[] {
    return [
      {
        id: 1,
        name: 'Muffin Nutnapin',
        topic: 'My new barbie',
        date: '13:40',
        detail: 'My new barbieMy new barbieMy new barbie',
      },
      {
        id: 2,
        name: 'Minnie Nichanun',
        topic: 'My new dinosaur',
        date: '13:40',
        detail: 'My new dinosaurMy new dinosaurMy new dinosaur',
      },
      {
        id: 3,
        name: 'Puk Puk',
        topic: 'Why everythings is so heavy',
        date: '13:40',
        detail:
          'Why everythings is so heavyWhy everythings is so heavyWhy everythings is so heavy',
      },
      {
        id: 4,
        name: 'Jackson Wang',
        topic: '100 Ways',
        date: '13:40',
        detail:
          '100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways',
      },
      {
        id: 5,
        name: 'Plaris Pornrat',
        topic: 'Puma is my cat',
        date: '13:40',
        detail: 'I love puma so much lala~~',
      },
      {
        id: 6,
        name: 'Muffin Nutnapin',
        topic: 'My new barbie',
        date: '13:40',
        detail: 'My new barbieMy new barbieMy new barbie',
      },
      {
        id: 7,
        name: 'Minnie Nichanun',
        topic: 'My new dinosaur',
        date: '13:40',
        detail: 'My new dinosaurMy new dinosaurMy new dinosaur',
      },
      {
        id: 8,
        name: 'Puk Puk',
        topic: 'Why everythings is so heavy',
        date: '13:40',
        detail:
          'Why everythings is so heavyWhy everythings is so heavyWhy everythings is so heavy',
      },
      {
        id: 9,
        name: 'Jackson Wang',
        topic: '100 Ways',
        date: '13:40',
        detail:
          '100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways',
      },
      {
        id: 10,
        name: 'Plaris Pornrat',
        topic: 'Puma is my cat',
        date: '13:40',
        detail: 'I love puma so much lala~~',
      },
      {
        id: 11,
        name: 'Muffin Nutnapin',
        topic: 'My new barbie',
        date: '13:40',
        detail: 'My new barbieMy new barbieMy new barbie',
      },
      {
        id: 12,
        name: 'Minnie Nichanun',
        topic: 'My new dinosaur',
        date: '13:40',
        detail: 'My new dinosaurMy new dinosaurMy new dinosaur',
      },
      {
        id: 13,
        name: 'Puk Puk',
        topic: 'Why everythings is so heavy',
        date: '13:40',
        detail:
          'Why everythings is so heavyWhy everythings is so heavyWhy everythings is so heavy',
      },
      {
        id: 14,
        name: 'Jackson Wang',
        topic: '100 Ways',
        date: '13:40',
        detail:
          '100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways',
      },
      {
        id: 15,
        name: 'Plaris Pornrat',
        topic: 'Puma is my cat',
        date: '13:40',
        detail: 'I love puma so much lala~~',
      },
      {
        id: 16,
        name: 'Muffin Nutnapin',
        topic: 'My new barbie',
        date: '13:40',
        detail: 'My new barbieMy new barbieMy new barbie',
      },
      {
        id: 17,
        name: 'Minnie Nichanun',
        topic: 'My new dinosaur',
        date: '13:40',
        detail: 'My new dinosaurMy new dinosaurMy new dinosaur',
      },
      {
        id: 18,
        name: 'Puk Puk',
        topic: 'Why everythings is so heavy',
        date: '13:40',
        detail:
          'Why everythings is so heavyWhy everythings is so heavyWhy everythings is so heavy',
      },
      {
        id: 19,
        name: 'Jackson Wang',
        topic: '100 Ways',
        date: '13:40',
        detail:
          '100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways100 Ways',
      },
      {
        id: 20,
        name: 'Plaris Pornrat',
        topic: 'Puma is my cat',
        date: '13:40',
        detail: 'I love puma so much lala~~',
      },
    ];
  }

  getMailId(id: number): Observable<MailData> {
    return of(this.getMailData().find((element) => element.id === id));
  }
}
